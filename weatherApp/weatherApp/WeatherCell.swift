//
//  weatherCell.swift
//  weatherApp
//
//  Created by Emir on 13/02/2019.
//  Copyright © 2019 Emir. All rights reserved.
//

import UIKit

class WeatherCell: UITableViewCell {
    
    @IBOutlet weak var weatherIcon: UIImageView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var weatherTypeLabel: UILabel!
    @IBOutlet weak var highTempLabel: UILabel!
    @IBOutlet weak var lowTempLabel: UILabel!
    
    func configureCell(forecast: Forecast) {
        lowTempLabel.text = "\(forecast.lowTemp)° C"
        highTempLabel.text = "\(forecast.highTemp)° C"
        weatherTypeLabel.text = forecast.weatherType
        dateLabel.text = forecast.date
    }
}
