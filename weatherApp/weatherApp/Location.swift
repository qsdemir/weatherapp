//
//  Location.swift
//  weatherApp
//
//  Created by Emir on 13/02/2019.
//  Copyright © 2019 Emir. All rights reserved.
//

import CoreLocation
import Alamofire

class Location {
    static var sharedInstance = Location()
    init() {}
    
    var latitude: Double!
    var longitude: Double!
    var _key: String!
    
    func downloadLocationData(completed: @escaping DownloadComplete) {
        let locationURL = URL(string: LOCATION_URL)!
        Alamofire.request(locationURL).responseJSON {
            response in let result = response.result
//            print("\(response)")
            guard let dict = result.value as? [String: AnyObject] else { return }
            guard let key = dict["Key"] as? String else { return }
            LOCATION_KEY = key
            
            completed()
        }
    }    
}
