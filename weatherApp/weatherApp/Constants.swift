//
//  Constants.swift
//  weatherApp
//
//  Created by Emir on 12/02/2019.
//  Copyright © 2019 Emir. All rights reserved.
//

import CoreLocation
import Foundation
import Alamofire

let BASE_URL = "http://api.openweathermap.org/data/2.5/weather?"
let FORECAST_BASE_URL = "http://dataservice.accuweather.com/forecasts/v1/daily/5day/"
let LATITUDE = "\(Location.sharedInstance.latitude ?? 0.0)"
let LONGITUDE = "\(Location.sharedInstance.longitude ?? 0.0)"
let APP_ID = "&appid="
let API_KEY = "01e725472ebd3e5fa7a166e149400fda"
let API2_KEY = "vAvZuyKdFAi8FHuJHRGykNJlE9BNgrKp"
var LOCATION_KEY = ""

typealias DownloadComplete = () -> Void

let CURRENT_WEATHER_URL =  "\(BASE_URL)lat=\(LATITUDE)&lon=\(LONGITUDE)\(APP_ID)\(API_KEY)"
let LOCATION_URL = "http://dataservice.accuweather.com/locations/v1/cities/geoposition/search?apikey=\(API2_KEY)&q=\(LATITUDE),\(LONGITUDE)"

let FORECAST_URL = "http://dataservice.accuweather.com/forecasts/v1/daily/5day/\(LOCATION_KEY)?apikey=\(API2_KEY)%20&metric=true"

//http://dataservice.accuweather.com/locations/v1/cities/geoposition/search?apikey=iUquG366mcT7xOFCEvJ5fFXfTUA9Ykoy&q=37.71%2C78.81

// https://samples.openweathermap.org/data/2.5/weather?lat=35&lon=139&appid=01e725472ebd3e5fa7a166e149400fda

//"api.openweathermap.org/data/2.5/forecast/daily?lat=35&lon=139&cnt=10&appid=01e725472ebd3e5fa7a166e149400fda"
