//
//  Forecast.swift
//  weatherApp
//
//  Created by Emir on 13/02/2019.
//  Copyright © 2019 Emir. All rights reserved.
//

import UIKit
import Alamofire

class Forecast{
    
    private(set) var forecasts: [Forecast] = []
    
    var _date: String!
    var _weatherType: String!
    var _highTemp: String!
    var _lowTemp: String!
    
    var date: String {
        if _date == nil {
            _date = ""
        }
        return _date
    }
    
    var weatherType: String {
        if _weatherType == nil {
            _weatherType = ""
        }
        return _weatherType
    }
    
    var highTemp: String {
        if _highTemp == nil {
            _highTemp = ""
        }
        return _highTemp
    }
    
    var lowTemp: String {
        if _lowTemp == nil {
            _lowTemp = ""
        }
        return _lowTemp
    }
    init() {}
    
    init(dictionary: Dictionary<String, AnyObject>) {
        guard let date = dictionary["Date"] as? String else { return }
        self._date = getDate(date: date)
        //  print("DATE: \(_date)")
        guard let temperature = dictionary["Temperature"] as? Dictionary<String, AnyObject> else { return }
        guard let minimum = temperature["Minimum"] as? Dictionary<String, AnyObject> else { return }
        guard let valueMin = minimum["Value"] as? Double else { return }
        self._lowTemp = "\(valueMin)"
        
        guard let maximum = temperature["Maximum"] as? Dictionary<String, AnyObject> else { return }
        guard let valueMax = maximum["Value"] as? Double else { return }
        self._highTemp = "\(valueMax)"
        
        guard let day = dictionary["Day"] as? [String: AnyObject] else { return }
        guard let type = day["IconPhrase"] as? String else { return }
        self._weatherType = type
    }
    
    func downloadForecastData(completed: @escaping DownloadComplete) {
        print("for: \(LOCATION_KEY)")
        let forecastURL = URL(string: FORECAST_URL)!      
        print("for: \(forecastURL)")
        Alamofire.request(forecastURL).responseJSON {
            response in let result = response.result
            guard let forecast = result.value as? Dictionary<String, AnyObject> else { return }
            guard let dailyForecasts = forecast["DailyForecasts"] as? [Dictionary<String, AnyObject>] else { return }
            for obj in dailyForecasts {
                let forec = Forecast(dictionary: obj)
                self.forecasts.append(forec)
            }
            completed()
        }
    }
    
    func getDate(date: String) -> String {
        let parts = date.split(separator: "T")
        let datum = parts[0].split(separator: "-")
        
        return "\(datum[2]). \(datum[1]). \(datum[0])."
    }
    
    
    
}
