//
//  CurrentWeather.swift
//  weatherApp
//
//  Created by Emir on 12/02/2019.
//  Copyright © 2019 Emir. All rights reserved.
//

import UIKit
import Alamofire

class CurrentWeather {
    private var _cityName: String!
    private var _date: String!
    private var _weatherType: String!
    private var _currentTemp: Double!
    private var _highTemp: Double!
    private var _location: Location!
    
    var cityName: String {
        if _cityName == nil {
            _cityName = ""
        }
        return _cityName
    }
    
    var weatherType: String{
        if _weatherType == nil {
            _weatherType = ""
        }
        return _weatherType
    }
    
    var currentTemp: Double{
        if _currentTemp == nil {
            _currentTemp = 0.0
        }
        return _currentTemp
    }
    
    var highTemp: Double{
        if _highTemp == nil {
            _highTemp = 0.0
        }
        return _highTemp
    }
    var date: String {
        if _date == nil {
            _date = ""
        }        
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .long
        dateFormatter.timeStyle = .none
        let currentDate = dateFormatter.string(from: Date())
        self._date = "Today, \(currentDate)"
        return _date
    }
        
    func downloadWeatherDetails(completed: @escaping DownloadComplete){
        let currentWeatherURL = URL(string: CURRENT_WEATHER_URL)!
        Alamofire.request(currentWeatherURL).responseJSON{ response in
            let result = response.result
            
            guard let dict = result.value as? Dictionary<String, AnyObject> else { return }
            if let name = dict["name"] as? String{
                self._cityName = name.capitalized
            }
            
            //                if let type = dict["weather"] as? [Dictionary<String, AnyObject>]{
            //                    if let main = type[0]["main"] as? String
            //                    {
            //                        self._weatherType = main
            //                    }
            //                }            
            guard let type = dict["weather"] as? [Dictionary<String, AnyObject>] else { return }
            guard let main = type[0]["main"] as? String else { return }
            self._weatherType = main
            
            if let temp = dict["main"] as? Dictionary<String, AnyObject>{
                if let _temp = temp["temp"] as? Double{
                    self._currentTemp = round(_temp - 273.15)
                }
            }
            
            
            if let highTemp = dict["main"] as? Dictionary<String, AnyObject>{
                if let _highTemp = highTemp["temp_max"] as? Double{
                    self._highTemp = round(_highTemp - 273.15)
                }
            }
            completed()
        }
    }
}
