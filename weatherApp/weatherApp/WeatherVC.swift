//
//  WeatherVC
//  weatherApp
//
//  Created by Emir on 12/02/2019.
//  Copyright © 2019 Emir. All rights reserved.
//

import UIKit
import CoreLocation

class WeatherVC: UIViewController, UITableViewDelegate, UITableViewDataSource, CLLocationManagerDelegate {
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var currentWeatherIcon: UIImageView!
    @IBOutlet weak var currentWeatherLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    let locationManager = CLLocationManager()
    var currentLocation: CLLocation!
    
    var currentWeather: CurrentWeather!
    var forecast: Forecast!
    var location: Location!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
       
        forecast = Forecast()
        location = Location()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        locationManager.requestWhenInUseAuthorization()
        locationManager.startMonitoringSignificantLocationChanges()
    }
    
    func loadData() {
        location.downloadLocationData() {
            self.forecast.downloadForecastData {
                self.tableView.reloadData()
            }
        }
        
        currentWeather = CurrentWeather()
        currentWeather.downloadWeatherDetails {
            self.UpdateMainUI()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .authorizedWhenInUse:
            locationAuthStatus()
        case .denied: break
        case .notDetermined: break
        case .restricted: break
        default:
            print("Default")
        }
    }
    
    func locationAuthStatus() {
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            currentLocation = locationManager.location
            Location.sharedInstance.latitude = currentLocation.coordinate.latitude
            Location.sharedInstance.longitude = currentLocation.coordinate.longitude
            loadData()
        } else {
            locationManager.requestWhenInUseAuthorization()
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return forecast.forecasts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "weatherCell", for: indexPath) as? WeatherCell {
            let fc = forecast.forecasts[indexPath.row]
            cell.configureCell(forecast: fc)
            return cell
        }
        return WeatherCell()
    }
    
    func UpdateMainUI() {        
        dateLabel.text = currentWeather.date
        temperatureLabel.text = "\(currentWeather.currentTemp)° C"
        locationLabel.text = currentWeather.cityName
        currentWeatherLabel.text = currentWeather.weatherType
        currentWeatherIcon.image = UIImage(named: currentWeather.weatherType)
    }
}
